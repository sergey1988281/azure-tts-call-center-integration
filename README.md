This repository contains texts of menus which automatically synthesis audio
files for Call Center IVR menu solution used by Service Desk team globally.

Repository contains python script to generate files as well as powershell 
deployment script and Azure Pipeline specification.