<#
.SYNOPSIS
    Generation and deployment of audio menu files from text.

.DESCRIPTION
    Deployment script reads menu xml files from git directory and trigger audio files
    generation only for changed ones. If branch is production files uploaded to SMB share.
#>
[CmdletBinding()]
Param (
  [Parameter(Mandatory=$true)]
  [string]$share_uname,
  [Parameter(Mandatory=$true)]
  [string]$share_pwd,
  [Parameter(Mandatory=$true)]
  [string]$share_url,
  [Parameter(Mandatory=$true)]
  [string]$azure_speech_key,
  [Parameter(Mandatory=$true)]
  [string]$azure_speech_region
)

$menupath = ".\menus"
$mediapath = ".\media"

if (-not (Test-Path -Path $mediapath -PathType Container)) {
  New-Item $mediapath -ItemType Directory
}

#
# Get only changed menus
#
# If last commit ID equals last merge ID - then check 2 commits back to identify real changes
if ( $(git log --merges --pretty="format:%h" -n1) -eq $(git log --pretty="format:%h" -n1) ) {
  Write-Host "Last commit was merge - checking 2 commits back"
  $changed_files = $(git diff-tree --no-commit-id --name-only -r `
                   $(git log --oneline -n1 --skip 1 --pretty=format:"%h")) `
                   -split ' ' | ForEach-Object { $_ -split "/" | Select-Object -Last 1 }
}
# Else - check just 1 commit back
else {
  Write-Host "Last commit wasnt merge - checking 1 commit back"
  $changed_files = $(git diff-tree --no-commit-id --name-only -r `
                   $(git log --oneline -n1 --pretty=format:"%h")) `
                   -split ' ' | ForEach-Object { $_ -split "/" | Select-Object -Last 1 }
}
$changed_menus = Get-ChildItem $menupath | Where-Object {($_.Name -like "*.xml") -and `
                                                         ($_.Name -in $changed_files)}
if (-not $changed_menus) {
  throw "No menus for resynthesis found"
}
Write-Host "Following menus were changed and will be resynthesised: $changed_menus"
# Resynthes changed menus
$xml = New-Object System.Xml.XmlDocument
foreach($menu in $changed_menus) {
  # XML integrity check
  try {
    $xml.Load($menu.FullName)
  }
  catch {
    throw "$($menu.Name) is invalid"
    break
  }
  #Speech synthesis
  foreach ($menu in $xml.Menus.ChildNodes) {
    $params = '.\speech.py', '--key', $azure_speech_key, '--region', $azure_speech_region, `
              '--file', ($mediapath + "\" + $menu.File.Trim())
    if($menu.ssml) {
      $params += '--ssml'
      $params += $menu.ssml.InnerXml.Replace('"','\"')
    }
    else {
      $params += '--text'
      $params += $menu.Text.Trim()
      if ($menu.Language) {
        $params += '--language'
        $params += $menu.Language.Trim()
      }
      if ($menu.Voice) {
        $params += '--voice'
        $params += $menu.Voice.Trim()
      }
    }
    python @params
  }
}
#
# Only for Production upload to share
#
if($env:BUILD_SOURCEBRANCHNAME -eq "Production") {
  $path_link = $share_url.Split("\")[2].Split("@")[0]
  Write-Host "Connecting to drive $share_url"
  if(-not (Test-NetConnection -Port 445 `
                              -ErrorAction SilentlyContinue $path_link).TcpTestSucceeded) {
    throw "Network connection to $path_link failed!"
  }
  try {
    $share_credentials = New-Object System.Management.Automation.PSCredential(
                                      $share_uname,
                                      (ConvertTo-SecureString $share_pwd -AsPlainText -Force))
    New-PSDrive -Name "Share" -Root $share_url -PSProvider "FileSystem" `
                -Credential $share_credentials -ErrorAction Stop
    Write-Host "Drive $share_url mounted!"
  }
  catch {
    throw "Unable to mount drive, exiting"
    break
  }
  # File copy logic
  $files = Get-ChildItem $mediapath
  foreach ($file in $files) {
    try {
      Copy-Item -Path $file.FullName -Destination ($share_url + "\" + $file) -ErrorAction Stop
      Write-Host "$file copied!"
    }
    catch {
      throw "Unable to copy file"
      break
    }
  }
}
