import sys
import getopt
try:
    import azure.cognitiveservices.speech as speechsdk
except ImportError:
    print("""
    Importing the Speech SDK for Python failed.
    Refer to:
    https://docs.microsoft.com/azure/cognitive-services/speech-service/quickstart-text-to-speech-python for
    installation instructions.
    """)
    sys.exit(1)
from typing import Union


def generate_voice(speech_key: str, service_region: str,
                   file_name: str, text: Union[None, str],
                   language: Union[None, str], voice: Union[None, str],
                   ssml: Union[None, str]) -> None:
    """
    Generate wav files using Azure Text to Speech service
    :param speech_key: Mandatory key from cognitive service resouse
    :param service_region: Mandatory Location of cognitive service resource
    :param file_name: Mandatory Name of file to generate voice
    :param text: Optional Text to generate Mandatory if SSML not specified
    :param language: Optional to specify language only valid with text
    :param voice: Optional to specify speaker only valid with
    :param ssml: Optional SSML instruction Mandatory if no text specified
    """
    # Instanciate of a speech config with subscription key and service region.
    speech_config = speechsdk.SpeechConfig(subscription=speech_key,
                                           region=service_region)
    speech_config.set_speech_synthesis_output_format(
        speechsdk.SpeechSynthesisOutputFormat['Riff8Khz16BitMonoPcm'])
    # Creates a speech synthesizer using file as audio output.
    file_config = speechsdk.audio.AudioOutputConfig(filename=file_name)
    if ssml is None:
        # If language wasnt explicitly specified using auto detection
        if language is None:
            print(f"Language wasnt specified - using autodetection. "
                  f"Please note it may fail if text amount is small")
            lang_config = speechsdk.languageconfig\
                .AutoDetectSourceLanguageConfig()
            speech_synthesizer = speechsdk.SpeechSynthesizer(
                speech_config=speech_config,
                audio_config=file_config,
                auto_detect_source_language_config=lang_config)
        # If language specified
        else:
            print("Language was specified:", language)
            speech_config.speech_synthesis_language = language
            # If voice specified
            if voice != '':
                print("Voice was specified:", voice)
                speech_config.speech_synthesis_voice_name = voice
            speech_synthesizer = speechsdk.SpeechSynthesizer(
                speech_config=speech_config,
                audio_config=file_config)
        result = speech_synthesizer.speak_text_async(text).get()
    else:
        speech_synthesizer = speechsdk.SpeechSynthesizer(
            speech_config=speech_config,
            audio_config=file_config)
        result = speech_synthesizer.speak_ssml_async(ssml).get()
        text = ssml
    # Check result
    if result.reason == speechsdk.ResultReason.SynthesizingAudioCompleted:
        print(f"Synthesized [{text}], saved to [{file_name}]".encode('utf-8'))
    elif result.reason == speechsdk.ResultReason.Canceled:
        cancellation_details = result.cancellation_details
        print(f"Speech synthesis canceled: {cancellation_details.reason}")
        error_msg = cancellation_details.reason
        if cancellation_details.reason == speechsdk.CancellationReason.Error:
            print(f"Error details: {cancellation_details.error_details}")
            error_msg += cancellation_details.error_details
        raise ValueError(error_msg)


if __name__ == "__main__":
    manual = (f"speech.py\n"
              f" -k --key <speech_service_key>\n"
              f" -r --region <speech_service_region>\n"
              f" -f --file <filename to save>\n"
              f" -t --text <text to synthese>\n"
              f" -l --language <synthesis_language>\n"
              f" -v --voice <synthesis_voice>\n"
              f" -s --ssml <Speech synthesis markup language string>")
    text = None
    language = None
    voice = None
    ssml = None
    # Reading arguments from command line
    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hk:r:t:f:l:v:s:",
            ["key=", "region=", "text=", "file=",
             "language=", "voice=", "ssml="])
    except getopt.GetoptError:
        print(manual)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print(manual)
            sys.exit()
        elif opt in ("-k", "--key"):
            speech_key = arg
        elif opt in ("-r", "--region"):
            service_region = arg
        elif opt in ("-t", "--text"):
            text = arg
        elif opt in ("-f", "--file"):
            file_name = arg
        elif opt in ("-l", "--language"):
            language = arg
        elif opt in ("-v", "--voice"):
            voice = arg
        elif opt in ("-s", "--ssml"):
            ssml = arg
    try:
        generate_voice(
            speech_key=speech_key,
            service_region=service_region,
            text=text,
            file_name=file_name,
            language=language,
            voice=voice,
            ssml=ssml
        )
    except Exception as err:
        print(err)
        sys.exit(1)
